## Movie Finder App

App lets user search for movie from two datasource (mongodb and OMDB). In mongoDb the data is inserted from the user side.

### Folder Structure and functionality

Core: Handles backend for the app. Its written in NodeJs with express and connected with mongoDb altas. The code insertes data in the db when it runs for 1st time.

WebApp: WebApp contains frontend of the app written in React(typescript), Redux and Saga.
User can search for a movie or view all the existing movie list in the database. On click on any movie user can view movie details.

### Unit Testing

Unit testing is done using chai-mocha

### Docker File

Each Folder have its own docker file to run
