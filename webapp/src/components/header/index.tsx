import "./index.scss";
import { routes } from "../../config/routes";
import { PageHeader, Tag } from "antd";
import React, { PureComponent } from "react";

interface HeaderProp {
  path: string;
}

interface HeaderState {
  title: string;
  subTitle: string;
}

class Header extends PureComponent<HeaderProp, HeaderState> {
  state = {
    title: "Movie Store",
    subTitle: "Search movie from database or from OMDB",
  };
  static getDerivedStateFromProps(props: HeaderProp, state: HeaderState) {
    if (props.path === routes.Search) {
      state.title = "Search Movies";
      state.subTitle = "Search and view movie from database or from OMDB";
    } else if (props.path === routes.All) {
      state.title = "Database Movies";
      state.subTitle = "List of all movies from database";
    }
    return state;
  }
  render() {
    return (
      <div className="header">
        <PageHeader
          title={this.state.title}
          subTitle={this.state.subTitle}
          tags={<Tag color="blue">Running</Tag>}
          avatar={{
            src: "https://avatars1.githubusercontent.com/u/8186664?s=460&v=4",
          }}
        ></PageHeader>
      </div>
    );
  }
}

export default Header;
