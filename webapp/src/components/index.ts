import Sider from "./sider";
import Tile from "./tile";
import Header from "./header";
import Content from "./content";

export { Sider, Tile, Header, Content };
