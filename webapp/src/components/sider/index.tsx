import "./index.scss";
import { Layout, Menu } from "antd";
import { routes } from "../../config/routes";
import React, { PureComponent } from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface SiderProp extends RouteComponentProps {
  path: string;
}

interface SiderState {
  active: string;
}

class Sider extends PureComponent<SiderProp, SiderState> {
  state = {
    active: "1",
  };
  static getDerivedStateFromProps(props: SiderProp, state: SiderState) {
    if (props.path === routes.Search) {
      state.active = "1";
    } else if (props.path === routes.All) {
      state.active = "2";
    }
    return state;
  }
  navigateTo = (location: string) => {
    this.props.history.push(location);
  };
  render() {
    console.log(this.state.active);
    return (
      <div className="sider">
        <Layout.Sider>
          <div className="logo">
            <img src={require("../../assests/logo.png")} alt="movie" />
          </div>
          <Menu theme="dark" defaultSelectedKeys={[this.state.active]}>
            <Menu.Item
              key="1"
              onClick={this.navigateTo.bind(this, routes.Search)}
            >
              Search Movie
            </Menu.Item>
            <Menu.Item key="2" onClick={this.navigateTo.bind(this, routes.All)}>
              All Database Movies
            </Menu.Item>
          </Menu>
        </Layout.Sider>
      </div>
    );
  }
}

export default withRouter(Sider);
