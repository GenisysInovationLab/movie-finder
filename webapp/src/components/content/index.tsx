import "./index.scss";
import { routes } from "../../config/routes";
import AllMovies from "../../pages/all-movies";
import SearchMovies from "../../pages/search-movie";
import { Route } from "react-router-dom";
import React, { PureComponent } from "react";

interface ContentProp {}

interface ContentState {}

class Content extends PureComponent<ContentProp, ContentState> {
  render() {
    return (
      <div className="content">
        <Route exact path={routes.All} component={AllMovies} />
        <Route exact path={routes.Search} component={SearchMovies} />
      </div>
    );
  }
}

export default Content;
