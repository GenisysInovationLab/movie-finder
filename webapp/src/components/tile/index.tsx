import "./index.scss";
import React, { PureComponent } from "react";
import { Typography, Tag, Tooltip } from "antd";
import { withRouter, RouteComponentProps } from "react-router";

interface TileProp extends RouteComponentProps {
  uid: string;
  title: string;
  imageUrl: string;
  rating: string;
  onClick: (id: string) => void;
}

interface TileState {
  moreOption: boolean;
}

class Tile extends PureComponent<TileProp, TileState> {
  getMovieInfo = () => {
    this.props.onClick(this.props.uid);
  };
  render() {
    return (
      <div className="zelp-tile" onClick={this.getMovieInfo}>
        <div>
          <div className="header-box">
            <img
              src={this.props.imageUrl}
              className="tile-icon"
              alt={this.props.title}
            />
          </div>
          <div className="body">
            <Tooltip title={this.props.title}>
              <Typography.Title level={4} ellipsis={{ rows: 1 }}>
                {this.props.title}
              </Typography.Title>
            </Tooltip>

            <Tag color="cyan">{this.props.rating}</Tag>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Tile);
