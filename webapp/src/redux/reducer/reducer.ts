import initialState, { State } from "./store";
import { IMovieAction } from "../actions/movie";

type ActionParams = IMovieAction;

const AppReducer = (
  state: State = initialState,
  actionParams: ActionParams
) => {
  switch (actionParams.type) {
    case "getMoviesViaApi":
      return {
        ...state,
        movies: actionParams.payload.movies,
      };
    case "getSearchResultViaAPi":
      if (actionParams.payload.search) {
        return {
          ...state,
          database: actionParams.payload.search.db,
          omdb: actionParams.payload.search.omdb,
        };
      } else return state;
    case "getMovieInfo":
      return {
        ...state,
        movieInfo: {},
      };
    case "movieInfo":
      if (actionParams.payload.searchResult) {
        return {
          ...state,
          movieInfo: actionParams.payload.searchResult,
        };
      } else return state;
    default:
      return state;
  }
};
export default AppReducer;
