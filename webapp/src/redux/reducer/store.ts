export interface Movies {
  _id: string;
  title: string;
  rating: string;
  release: string;
  actors: string;
  plot: string;
  imageUrl: string;
  omdbId: string;
  created: Date;
}

export interface OMDB {
  Poster: string;
  Title: string;
  Type: string;
  Year: string;
  imdbID: string;
}

export interface State {
  movies: Movies[];
  database: Movies[];
  omdb: OMDB[];
  movieInfo: Movies;
}

const initialState: State = {
  movies: [],
  database: [],
  omdb: [],
  movieInfo: {
    _id: "",
    title: "",
    rating: "",
    release: "",
    actors: "",
    plot: "",
    imageUrl: "",
    omdbId: "",
    created: new Date(),
  },
};

export default initialState;
