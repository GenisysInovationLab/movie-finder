import { Dispatch } from "redux";
import { State, Movies, OMDB } from "../reducer/store";

type Actions =
  | "getAllMovies"
  | "getMoviesViaApi"
  | "searchMovie"
  | "getSearchResultViaAPi"
  | "getMovieInfo"
  | "movieInfo";

type Payload = {
  prefix: string;
  limit?: number;
  offset?: number;
  flag?: "all" | "db" | "omdb";
  movies: Movies[];
  search?: {
    db: Movies[];
    omdb: OMDB[];
  };
  omdbId?: string;
  searchResult?: Movies;
};

export interface IMovieAction {
  type: Actions;
  payload: Payload;
}

function Movie(params: IMovieAction) {
  return {
    type: params.type,
    payload: params.payload,
  };
}

export interface ReduxMovieAction {
  MovieAction: (params: IMovieAction) => IMovieAction;
}

export function mapDispatchToProps(dispatch: Dispatch): ReduxMovieAction {
  return {
    MovieAction: (params: IMovieAction) => dispatch(Movie(params)),
  };
}

export interface ReduxMovieState {
  Movie: State["movies"];
  Database: State["database"];
  OMDB: State["omdb"];
  MovieInfo: State["movieInfo"];
}

export function mapStateToProps(state: State): ReduxMovieState {
  return {
    Movie: state.movies,
    Database: state.database,
    OMDB: state.omdb,
    MovieInfo: state.movieInfo,
  };
}
