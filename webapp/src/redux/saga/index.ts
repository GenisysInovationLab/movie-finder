import { notification } from "antd";
import { baseUrl } from "../../config/baseUrl";
import { Movies } from "../reducer/store";
import { IMovieAction } from "../actions/movie";
import { all, takeLatest, put } from "redux-saga/effects";

function* getAllMovies() {
  try {
    const response: Movies[] = yield fetch(`${baseUrl}/movies`)
      .then((res) => res.json())
      .catch((err) => console.log(err));
    if (response) {
      yield put({
        type: "getMoviesViaApi",
        payload: {
          movies: response,
        },
      });
    }
  } catch (error) {
    console.log("Error while fetching movie list ", error);
  }
}

function* searchMovie(params: IMovieAction) {
  try {
    const response: any = yield fetch(
      `${baseUrl}/autocomplete?prefix=${params.payload.prefix}&limit=${params.payload.limit}&offset=${params.payload.offset}&flag=${params.payload.flag}`
    )
      .then((res) => res.json())
      .catch((err) => console.log(err));
    console.log(response);
    if (response.status === 200) {
      yield put({
        type: "getSearchResultViaAPi",
        payload: {
          search: {
            db: response.dbResult,
            omdb: response.omdbResult,
          },
        },
      });
    } else if (response.status === 400) {
      notification.error({
        message: response.message,
        description: response.error,
      });
    }
  } catch (error) {
    console.log("Error while fetching movie list ", error);
  }
}

function* getMovieInfo(params: IMovieAction) {
  try {
    const response: Movies = yield fetch(
      `${baseUrl}/movies/${params.payload.omdbId}`
    )
      .then((res) => res.json())
      .catch((err) => console.log(err));
    if (response) {
      yield put({
        type: "movieInfo",
        payload: {
          searchResult: response,
        },
      });
    }
  } catch (error) {
    console.log("Error while fetching movie details ", error);
  }
}

function* actionWatcher() {
  yield takeLatest("getAllMovies", getAllMovies);
  yield takeLatest("searchMovie", searchMovie);
  yield takeLatest("getMovieInfo", getMovieInfo);
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
