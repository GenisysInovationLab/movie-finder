import "./index.scss";
import { Layout } from "antd";
import { routes } from "../../config/routes";
import { Sider, Header, Content } from "../../components";
import React, { PureComponent } from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface HomeProp extends RouteComponentProps {}

interface HomeState {
  path: string;
}

class Home extends PureComponent<HomeProp, HomeState> {
  state = {
    path: "/",
  };
  static getDerivedStateFromProps(props: HomeProp, state: HomeState) {
    console.log(props.location.pathname);
    switch (props.location.pathname) {
      case routes.Search:
        state.path = routes.Search;
        break;
      case routes.All:
        state.path = routes.All;
        break;
      default:
        state.path = routes.Search;
    }
    return state;
  }
  render() {
    return (
      <div className="home">
        <Layout>
          <Sider path={this.state.path} />
          <Layout className="right-panel">
            <Header path={this.state.path} />
            <Content />
          </Layout>
        </Layout>
      </div>
    );
  }
}

export default withRouter(Home);
