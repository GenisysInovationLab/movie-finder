import "./index.scss";
import { connect } from "react-redux";
import {
  Input,
  Button,
  Typography,
  Modal,
  Tag,
  Popover,
  Radio,
  Empty,
} from "antd";
import React, { PureComponent } from "react";
import {
  ReduxMovieAction,
  ReduxMovieState,
  mapDispatchToProps,
  mapStateToProps,
} from "../../redux/actions/movie";
import { Tile } from "../../components";

interface SearchMovieProp extends ReduxMovieAction, ReduxMovieState {}

interface SearchMovieState {
  prefix: string;
  limit: number;
  offset: number;
  flag: "all" | "db" | "omdb";
  movieInfo: boolean;
}

class SearchMovie extends PureComponent<SearchMovieProp, SearchMovieState> {
  constructor(props: SearchMovieProp) {
    super(props);
    this.state = {
      prefix: "",
      limit: 5,
      offset: 0,
      flag: "all",
      movieInfo: false,
    };
  }
  toggleMovieInfo = () => {
    this.setState({
      movieInfo: !this.state.movieInfo,
    });
  };
  searchMovie = () => {
    this.props.MovieAction({
      type: "searchMovie",
      payload: {
        prefix: this.state.prefix,
        offset: this.state.offset,
        limit: this.state.limit,
        flag: this.state.flag,
        movies: [],
      },
    });
  };
  handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(
      {
        prefix: event.target.value,
      },
      () => {
        this.searchMovie();
      }
    );
  };
  getMovieDetails = (movieId: string) => {
    this.toggleMovieInfo();
    console.log(movieId);

    this.props.MovieAction({
      type: "getMovieInfo",
      payload: {
        prefix: "",
        movies: [],
        omdbId: movieId,
      },
    });
  };
  handleLimit = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      limit: parseInt(event.target.value),
    });
  };
  handleOffset = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      offset: parseInt(event.target.value),
    });
  };
  handleFlag = (event: any) => {
    this.setState({
      flag: event.target.value,
    });
  };
  searchFilter = () => {
    return (
      <div className="search-filter">
        <div className="row">
          <Typography.Text strong>Limit</Typography.Text>
          <input
            type="number "
            value={this.state.limit}
            onChange={this.handleLimit}
          />
        </div>
        <div className="row">
          <Typography.Text strong>Offset</Typography.Text>
          <input
            type="number "
            value={this.state.offset}
            onChange={this.handleOffset}
          />
        </div>
        <div className="row">
          <Typography.Text strong>Source</Typography.Text>
          <Radio.Group
            defaultValue={this.state.flag}
            buttonStyle="solid"
            onChange={this.handleFlag}
          >
            <Radio.Button value="db">Database</Radio.Button>
            <Radio.Button value="omdb">OMDB</Radio.Button>
            <Radio.Button value="all">Both</Radio.Button>
          </Radio.Group>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div className="search-movie">
        <div className="search-box">
          <Input.Search
            value={this.state.prefix}
            placeholder="Type movie name to search"
            size="large"
            className="search-input"
            onChange={this.handleSearch}
            onPressEnter={this.searchMovie}
          />
          <Popover
            placement="bottom"
            trigger="hover"
            content={<this.searchFilter />}
          >
            <Button icon="filter" size="large" />
          </Popover>
        </div>
        <div className="result">
          <div className="database">
            <Typography.Text strong className="result-title">
              From Database
            </Typography.Text>
            <div>
              {this.props.Database.map((item) => {
                return (
                  <Tile
                    key={item._id}
                    uid={item.omdbId}
                    title={item.title}
                    imageUrl={item.imageUrl}
                    rating={item.rating}
                    onClick={this.getMovieDetails}
                  />
                );
              })}
            </div>
            {this.props.Database.length ? null : (
              <div>
                <Empty description={"You do not have any search result"} />
              </div>
            )}
          </div>
          <div className="omdb">
            <Typography.Text strong className="result-title">
              From OMDB
            </Typography.Text>
            <div>
              {this.props.OMDB.map((item, index) => {
                return (
                  <Tile
                    key={index}
                    uid={item.imdbID}
                    title={item.Title}
                    imageUrl={item.Poster}
                    rating={item.Type}
                    onClick={this.getMovieDetails}
                  />
                );
              })}
            </div>
            {this.props.OMDB.length ? null : (
              <div>
                <Empty
                  description={
                    "Note: Minimum 3 letter required for searching in OMDB database "
                  }
                />
              </div>
            )}
          </div>
        </div>
        <Modal
          visible={this.state.movieInfo}
          title="Movie Details"
          width="700px"
          footer={null}
          destroyOnClose
          onCancel={this.toggleMovieInfo}
        >
          <div className="movie-info">
            <div className="poster">
              <img
                src={this.props.MovieInfo.imageUrl}
                alt={this.props.MovieInfo.title}
                height="400px"
              />
            </div>
            <div className="info">
              <div className="row">
                <Typography.Text strong>Title</Typography.Text>
                <Typography.Text>{this.props.MovieInfo.title}</Typography.Text>
              </div>
              <div className="row">
                <Typography.Text strong>Plot</Typography.Text>
                <Typography.Paragraph ellipsis={{ rows: 2, expandable: true }}>
                  {this.props.MovieInfo.plot}
                </Typography.Paragraph>
              </div>
              <div className="row">
                <Typography.Text strong>Rating</Typography.Text>
                <Tag color="cyan" style={{ width: "35px" }}>
                  {this.props.MovieInfo.rating}
                </Tag>
              </div>
              <div className="row">
                <Typography.Text strong>Actors</Typography.Text>
                <Typography.Text>{this.props.MovieInfo.actors}</Typography.Text>
              </div>
              <div className="row">
                <Typography.Text strong>Release Date</Typography.Text>
                <Typography.Text>
                  {this.props.MovieInfo.release}
                </Typography.Text>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchMovie);
