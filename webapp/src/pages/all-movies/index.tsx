import "./index.scss";
import { Modal, Typography, Tag } from "antd";
import { connect } from "react-redux";
import { Tile } from "../../components";
import React, { PureComponent } from "react";
import {
  ReduxMovieAction,
  ReduxMovieState,
  mapDispatchToProps,
  mapStateToProps,
} from "../../redux/actions/movie";

interface AllMoviesProp extends ReduxMovieAction, ReduxMovieState {}

interface AllMoviesState {
  movieInfo: boolean;
}

class AllMovies extends PureComponent<AllMoviesProp, AllMoviesState> {
  state = {
    movieInfo: false,
  };
  toggleMovieInfo = () => {
    this.setState({
      movieInfo: !this.state.movieInfo,
    });
  };
  componentDidMount() {
    this.props.MovieAction({
      type: "getAllMovies",
      payload: {
        prefix: "",
        movies: [],
      },
    });
  }
  getMovieDetails = (movieId: string) => {
    this.toggleMovieInfo();
    this.props.MovieAction({
      type: "getMovieInfo",
      payload: {
        prefix: "",
        movies: [],
        omdbId: movieId,
      },
    });
  };
  render() {
    return (
      <div className="AllMovies">
        {this.props.Movie.map((item) => {
          return (
            <Tile
              key={item._id}
              uid={item.omdbId}
              title={item.title}
              imageUrl={item.imageUrl}
              rating={item.rating}
              onClick={this.getMovieDetails}
            />
          );
        })}
        <Modal
          visible={this.state.movieInfo}
          title="Movie Details"
          width="700px"
          footer={null}
          destroyOnClose
          onCancel={this.toggleMovieInfo}
        >
          <div className="movie-info">
            <div className="poster">
              <img
                src={this.props.MovieInfo.imageUrl}
                alt={this.props.MovieInfo.title}
                height="400px"
              />
            </div>
            <div className="info">
              <div className="row">
                <Typography.Text strong>Title</Typography.Text>
                <Typography.Text>{this.props.MovieInfo.title}</Typography.Text>
              </div>
              <div className="row">
                <Typography.Text strong>Plot</Typography.Text>
                <Typography.Paragraph ellipsis={{ rows: 2, expandable: true }}>
                  {this.props.MovieInfo.plot}
                </Typography.Paragraph>
              </div>
              <div className="row">
                <Typography.Text strong>Rating</Typography.Text>
                <Tag color="cyan" style={{ width: "35px" }}>
                  {this.props.MovieInfo.rating}
                </Tag>
              </div>
              <div className="row">
                <Typography.Text strong>Actors</Typography.Text>
                <Typography.Text>{this.props.MovieInfo.actors}</Typography.Text>
              </div>
              <div className="row">
                <Typography.Text strong>Release Date</Typography.Text>
                <Typography.Text>
                  {this.props.MovieInfo.release}
                </Typography.Text>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllMovies);
