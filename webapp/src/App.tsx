import "./index.scss";
import Home from "./pages/home";
import React, { PureComponent } from "react";

interface AppProp {}

interface AppState {}

class App extends PureComponent<AppProp, AppState> {
  render() {
    return (
      <div className="app">
        <Home />
      </div>
    );
  }
}

export default App;
