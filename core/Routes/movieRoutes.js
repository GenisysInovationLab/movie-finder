"use strict";


const express = require('express');
const movie = require('../Controller/movie');
const msg = require('../Config/messageConfig');

var routes = function () {
    var movieRouter = express.Router();
    movieRouter.route("/movies")
        .get(function (req, res) {
            movie.getAllMovies((response) => {
                res.json(response);
            })
        })
    movieRouter.route('/autocomplete')
        .get(function (req, res) {
            const params = req.query;
            /**
             * API Validation 
             */
            if (!params.prefix) {
                res.json(
                    { ...msg.invalidParams, error: "prefix is required params" }
                );
            } else if (params.limit < 0) {
                res.json(
                    { ...msg.invalidParams, error: "limit cannot be negative number" }
                );
            } else if (params.offset < 0) {
                res.json(
                    { ...msg.invalidParams, error: "offset cannot be negative number" }
                );
            } else if (params.offset >= params.prefix.length) {
                res.json(
                    { ...msg.invalidParams, error: "offset cannot be greater or equal to prefix length" }
                );
            } else if (params.flag && !["db", "omdb", "all"].includes(params.flag)) {
                res.json(
                    { ...msg.invalidParams, error: "invalid value for flag" }
                );
            }
            /** 
             * API Processing 
            */
            else {
                const prefix = params.prefix;
                const limit = params.limit || 5;
                const offset = params.offset || 0;
                const flag = params.flag || "all";
                const sliceResult = prefix.slice(offset, prefix.length);
                movie.getMovies(sliceResult, limit, flag, (response) => {
                    res.send(response);
                });
            }
        })
    movieRouter.route("/movies/:movieId")
        .get(function (req, res) {
            const id = (req.params.movieId);
            movie.getMovieById(id, (response) => {
                res.json(response);
            })
        })

    return movieRouter;
};

module.exports = routes;