const msg = {
    databaseErr: {
        status: 400,
        message: "Error occured while accessing MongoDb!"
    },
    invalidParams: {
        status: 400,
        message: "Required params are missing"
    },
    invalidUrl: {
        status: 400,
        message: "Entered URL is not found, Please enter correct URL!"
    },
    movieNotFound: {
        status: 400,
        message: "Requested movie is not found"
    }
}

module.exports = msg;