const omdb = {
    baseUrl: "http://www.omdbapi.com/",
    key: "f7521285"
}

const apiString = `${omdb.baseUrl}?apikey=${omdb.key}`;

module.exports = apiString;