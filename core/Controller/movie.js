"use strict";
const fetch = require('node-fetch');
const movieModel = require('../Model/movie');
const Msg = require('../Config/messageConfig');
const omdbUrl = require('../Config/baseUrl');

var methods = {
    getAllMovies: function (callback) {
        movieModel.find({}, (err, docs) => {
            if (err) {
                console.log("Error while fetching data");
                callback([])
            } else {
                callback(docs)
            }
        })
    },
    dbSearch: function (string, limit, callback) {
        movieModel.find({ title: { $regex: `${string}`, $options: "i" } }).limit(parseInt(limit)).exec((err, docs) => {
            if (err) {
                console.log("Error while accessing db ", err);
                callback(Msg.databaseErr);
            } else {
                callback(docs);
            }
        });
    },
    omdbSearch: function (string, callback) {
        fetch(`${omdbUrl}&s=${string}`)
            .then(res => res.json())
            .then(data => {
                if (data.Search) {
                    callback(data.Search);

                } else {
                    callback([]);
                }
            })
            .catch(e => {
                console.log("Error while fetching omdb movie ", e);
                callback([]);
            });
    },
    getMovies: function (prefix, limit, source, callback) {
        let dbResult = [];
        if (source === "all") {
            this.dbSearch(prefix, limit, (result) => {
                dbResult = result;
                this.omdbSearch(prefix, (response) => {
                    callback({
                        status: 200,
                        dbResult: dbResult,
                        omdbResult: response
                    })
                });
            });

        } else if (source === "db") {
            this.dbSearch(prefix, (result) => {
                callback({
                    status: 200,
                    dbResult: result,
                    omdbResult: []
                });
            });

        } else if (source === "omdb") {
            this.omdbSearch(prefix, (result) => {
                callback({
                    status: 200,
                    omdbResult: result,
                    dbResult: []
                });
            });
        }
    },
    getMovieById: function (id, callback) {
        fetch(`${omdbUrl}&i=${id}`)
            .then(res => res.json())
            .then(data => {
                if (data) {
                    callback({
                        title: data.Title,
                        rating: data.imdbRating,
                        release: data.Released,
                        actors: data.Actors,
                        plot: data.Plot,
                        imageUrl: data.Poster,
                        omdbId: data.imdbID
                    });

                } else {
                    callback(Msg.movieNotFound);
                }
            })
            .catch(e => {
                console.log("Error while fetching omdb movie ", e);
                callback(callback(Msg.movieNotFound));
            });
    }
}

module.exports = methods;