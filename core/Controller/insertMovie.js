"use strict";
const mongoose = require('mongoose');
const movieSchema = require("../Model/movie");
const fetch = require('node-fetch');
const omdbUrl = require("../Config/baseUrl");
const movieList = ["iron", "i am", "i am ali", "i am another you", "i am eleven", "i am i", "ice age", "ice man", "ida", "identity", "Invincible", "In America", "Inkheart"];

const methods = {
    fetchMovie: function (movieName) {
        fetch(`${omdbUrl}&t=${movieName}`)
            .then(res => res.json())
            .then(data => {
                const newMovie = new movieSchema({
                    title: data.Title,
                    rating: data.imdbRating,
                    release: data.Released,
                    actors: data.Actors,
                    plot: data.Plot,
                    imageUrl: data.Poster,
                    omdbId: data.imdbID
                });
                newMovie.save((err) => {
                    if (err) {
                        console.log("Error while inserting data in database");
                    } else {
                        console.log("Data inserted successfully for ", movieName);
                    }
                })
            })
            .catch(e => {
                console.log("Error while fetching omdb movie ", e);
            });
    },
    insertMovies: function () {
        mongoose.connection.db.listCollections({
            name: 'movies'
        })
            .next((err, result) => {
                if (err) {
                    console.log(err);
                } else {
                    if (result) {
                        console.log("Data available for movies collections");
                    } else {
                        movieList.forEach(item => {
                            this.fetchMovie(item);
                        });
                    }
                }
            })
    }
}

module.exports = methods;



