const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();

chai.use(chaiHttp);

describe("/Get Autocomplete without prefix", () => {
    it("it should ask user to provide movie prefix", (done) => {
        chai.request(server)
            .get("/autocomplete")
            .query({})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message');
                res.body.should.have.property('error');
                done();
            })
    })
})

describe("/Get Autocomplete with negative limit", () => {
    it("it should throw error as limit value should be positive", (done) => {
        chai.request(server)
            .get("/autocomplete")
            .query({ limit: -3, prefix: "iron" })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message');
                res.body.should.have.property('error');
                done();
            })
    })
})

describe("/Get Autocomplete with negative offset", () => {
    it("it should throw error as offset value should be positive", (done) => {
        chai.request(server)
            .get("/autocomplete")
            .query({ offset: -3, prefix: "iron" })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message');
                res.body.should.have.property('error');
                done();
            })
    })
})

describe("/Get Autocomplete with offset value greater then prefix length", () => {
    it("it should throw error as offset value should less then prefix length", (done) => {
        chai.request(server)
            .get("/autocomplete")
            .query({ offset: 5, prefix: "iron" })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message');
                res.body.should.have.property('error');
                done();
            })
    })
})

describe("/Get Autocomplete with flag not in [all, db, omdb]", () => {
    it("it should throw error as flag should be in [all, db, omdb]", (done) => {
        chai.request(server)
            .get("/autocomplete")
            .query({ flag: "mySql", prefix: "iron" })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message');
                res.body.should.have.property('error');
                done();
            })
    })
})
