"use strict";

const mongoose = require('mongoose');
const schema = mongoose.Schema;

const Movie = new schema({
    title: String,
    rating: String,
    release: String,
    actors: String,
    plot: String,
    imageUrl: String,
    omdbId: String,
    created: { type: Date, default: Date.now }

}, { versionKey: false });

const movies = mongoose.model("movies", Movie);
module.exports = movies;

